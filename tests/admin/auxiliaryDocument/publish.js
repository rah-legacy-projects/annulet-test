require('../../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'auxiliaryDocument admin publish': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update(),
            testUtility.admin.auxiliaryDocument.publish()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.admin.auxiliaryDocument.published);
            logger.silly('[top] ' + util.inspect(p.admin.auxiliaryDocument.published));
            test.ok(p.admin.auxiliaryDocument.published.rangedData);
            test.equal(p.admin.auxiliaryDocument.published.rangedData.length, 1);
            test.equal(p.admin.auxiliaryDocument.published.rangedData[0].description, 'test description');
            test.equal(p.admin.auxiliaryDocument.published.rangedData[0].displayName, 'test auxiliaryDocument');
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
