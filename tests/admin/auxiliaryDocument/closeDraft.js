require('../../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'close auxdox draft': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),

            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update(),
            testUtility.admin.auxiliaryDocument.publish(),
            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update({
                displayName: 'open draft',
                description: 'open draft'
            }),
            testUtility.admin.auxiliaryDocument.list(),
            function(p, cb) {
                logger.silly(new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.admin.auxiliaryDocument.close()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.admin.auxiliaryDocument.close);
            test.equal(p.admin.auxiliaryDocument.close, p.admin.auxiliaryDocument.draft._id);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
