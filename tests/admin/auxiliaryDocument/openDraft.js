require('../../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'auxiliaryDocument admin list': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),

            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update(),
            testUtility.admin.auxiliaryDocument.publish(),
            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update({
                displayName: 'open draft',
                description: 'open draft'
            }),
            function(p, cb) {
                logger.silly(new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.admin.auxiliaryDocument.list(),
        ], function(err, p) {
            logger.silly(JSON.stringify(p.admin.auxiliaryDocument.list, null, 3));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.admin.auxiliaryDocument.list);
            test.equal(p.admin.auxiliaryDocument.list.length, 2);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
