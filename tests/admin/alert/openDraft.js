require('../../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'alert admin list': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),

            testUtility.admin.alert.draft(),
            testUtility.admin.alert.update(),
            testUtility.admin.alert.publish(),
            testUtility.admin.alert.draft(),
            testUtility.admin.alert.update({
                displayName: 'open draft',
                description: 'open draft'
            }),
            function(p, cb) {
                logger.silly(new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.admin.alert.list(),
        ], function(err, p) {
            logger.silly(JSON.stringify(p.admin.alert.list, null, 3));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.admin.alert.list);
            test.equal(p.admin.alert.list.length, 2);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
