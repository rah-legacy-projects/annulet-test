module.exports = exports = {
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	//operatingProcedure: require("./operatingProcedure"),
	workflow: require("./workflow"),
};
