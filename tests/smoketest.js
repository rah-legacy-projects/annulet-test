require('../test-config');
var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    modeler = require('annulet-models').modeler,
    testUtility = require("../util");

module.exports = exports = {
    setUp: function(cb){
        var self = this;
        testUtility.setupHelper(function(err, p){
            self.p = p;
            cb(err);
        });
    },
    'smoketest': function(test){
        test.ok('heartbeat');
        test.done();
    },
    tearDown:function(cb){
        var self = this;
        testUtility.endServices(self.p.services, function(err, p){
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
