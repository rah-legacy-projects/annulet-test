require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list workflows after update': function(test) {

        var bar = function(num) {
            return function(p, cb) {
                logger.silly(' ## ' + num + ' ' + new Array(96)
                    .join('#'));
                cb(null, p);
            };
        };

        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            testUtility.admin.workflow.list(),
            function(p, cb) {
                testUtility.admin.workflow.draft({
                    workflowDefinition: p.admin.workflow.list[0].published._id
                })(p, cb);
            },
            function(p, cb) {
                testUtility.admin.workflow.update({
                    workflowDefinition: p.admin.workflow.draft._id,
                    workflowDefinitionRange: p.admin.workflow.draft._rangeId,
                    endDate:'forever',
                    startDate:'forever',
                    title: 'altered title'
                })(p, cb);
            },
            function(p, cb) {
                testUtility.admin.workflow.publish({
                    workflowDefinition: p.admin.workflow.draft._id
                })(p, cb);
            },
            testUtility.workflow.get(),
            testUtility.admin.workflow.list(),
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.workflow.get));
            logger.silly('[top] ' + util.inspect(p.admin.workflow.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.equal(p.workflow.get.title, 'altered title');
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
