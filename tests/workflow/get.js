require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    ObjectId = require('mongoose').Types.ObjectId,
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'get workflow': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.workflow.draft(),
            testUtility.admin.workflow.update(),
            testUtility.admin.workflow.publish(),
            testUtility.workflow.list(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.workflow.get()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.workflow.list);
            //all new customers, for now, get the base one + the one just drafted
            test.equal(p.workflow.list.length, 2);
            test.equal(p.workflow.get.itemDefinition, p.workflow.list[0].definitionId);
            logger.silly('workflow: ' + JSON.stringify(p.workflow.get, null, 2));
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }

};
