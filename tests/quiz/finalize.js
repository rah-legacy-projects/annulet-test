require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'finalize quiz, wf before': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            function(p, cb){
                logger.silly('## 1 ' + new Array(96).join('#'));
                cb(null, p);
            },

            testUtility.quiz.get(),
            function(p, cb){
                logger.silly('## 2 ' + new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.quiz.answerQuestions(),
            testUtility.quiz.finalize(),
            testUtility.workflow.get(),
        ], function(err, p) {
            logger.silly('[top] ' + JSON.stringify(p.workflow.get,null,3));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.equal(p.quiz.finalize.workflowItem, p.workflow.get._id);
            test.ok(p.workflow.get.items[0].sequence[1].completedDate);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }

};
