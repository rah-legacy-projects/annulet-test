require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list quizs after update': function(test) {

        var bar = function(num) {
            return function(p, cb) {
                logger.silly(' ## ' + num + ' ' + new Array(96)
                    .join('#'));
                cb(null, p);
            };
        };

        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            testUtility.quiz.get(),
            bar(1),
            testUtility.admin.quiz.list(),
            bar(2),
            function(p, cb) {
                logger.silly('admin quiz list: ' + util.inspect(p.admin.quiz.list));
                testUtility.admin.quiz.draft({
                    quizDefinition: p.admin.quiz.list[0].published._id
                })(p, cb);
            },
            bar(3),
            function(p, cb) {
                testUtility.admin.quiz.update({
                    quizDefinition: p.admin.quiz.draft._id,
                    quizDefinitionRange: p.admin.quiz.draft._rangeId,
                    endDate:'forever',
                    startDate:'forever',
                    title: 'altered title'
                })(p, cb);
            },
            bar(4),
            testUtility.admin.quiz.list(),
            function(p, cb){
                logger.silly('admin quiz list: ' + util.inspect(p.admin.quiz.list));
                cb(null, p);
            },
            bar(5),
            function(p, cb) {
                testUtility.admin.quiz.publish({
                    quizDefinition: p.admin.quiz.draft._id
                })(p, cb);
            },
            bar(6),
            testUtility.workflow.get(),
            bar(7),
            testUtility.admin.quiz.list(),
            bar(8),
            testUtility.quiz.get(),
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.quiz.get));
            logger.silly('[top] ' + util.inspect(p.admin.quiz.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.equal(p.quiz.get.quizDefinitionRange.title, 'altered title');
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
