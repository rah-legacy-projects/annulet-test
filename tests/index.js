module.exports = exports = {
	admin: require("./admin"),
	alert: require("./alert"),
	auxiliaryDocument: require("./auxiliaryDocument"),
	login: require("./login"),
	operatingProcedure: require("./operatingProcedure"),
	quickView: require("./quickView"),
	quiz: require("./quiz"),
	signup: require("./signup"),
	smoketest: require("./smoketest"),
	training: require("./training"),
	workflow: require("./workflow"),
};
