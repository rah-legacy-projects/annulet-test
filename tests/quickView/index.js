module.exports = exports = {
	adminOutstandingOPs: require("./adminOutstandingOPs"),
	userOperatingProcedures: require("./userOperatingProcedures"),
	userWorkflows: require("./userWorkflows"),
};
