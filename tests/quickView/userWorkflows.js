require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'quickview workflows': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.quickView.userWorkflows()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.workflowStatusList);
            //hack: for now, assume one
            test.equal(p.workflowStatusList.length, 1)
            logger.silly('[top] ' + util.inspect(p.workflowStatusList[0]));

            test.equal(p.workflowStatusList[0].title, 'Test Workflow');

            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
