require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list trainings after delete': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            testUtility.training.get(),
            function(p, cb) {
                logger.silly(' ## 1 ' + new Array(96)
                    .join('#'));
                cb(null, p);
            },

            testUtility.admin.training.list(),
             function(p, cb) {
                logger.silly(' ## 2' + new Array(96)
                    .join('#'));
                cb(null, p);
            },
            function(p, cb) {
                //close the published draft
                testUtility.admin.training.close({
                    trainingDefinition: p.admin.training.list[0].published._id
                })(p, cb);
            },

            function(p, cb) {
                logger.silly(' ## 3 ' + new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.workflow.get()
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.workflow.get.items[0].sequence[1]));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
