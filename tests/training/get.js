require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'get training, wf before': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.training.get()
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.training.get));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.equal(p.training.get.trainingDefinition, p.workflow.get.items[0].sequence[0].training);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    },
    'get training twice, wf before': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.workflow.list(),
            testUtility.workflow.get(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.training.get(),
            testUtility.training.get()
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.training.get));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.equal(p.training.get.trainingDefinition, p.workflow.get.items[0].sequence[0].training);
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }

};
