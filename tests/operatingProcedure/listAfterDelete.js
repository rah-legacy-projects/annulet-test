require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list operatingProcedures after delete': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.operatingProcedure.draft(),
            testUtility.admin.operatingProcedure.update(),
            testUtility.admin.operatingProcedure.publish(),
            testUtility.operatingProcedure.list(),
            function(p, cb) {
                logger.silly(JSON.stringify(p, null, 3));
                //close the published draft
                testUtility.admin.operatingProcedure.close({
                    operatingProcedureDefinition: p.admin.operatingProcedure.published._id
                })(p, cb);
            },

            function(p, cb) {
                logger.silly(new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.operatingProcedure.list(),
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.operatingProcedure.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.operatingProcedure.list);
            //1 from stamp + 1 published - 1 deleted
            test.equal(p.operatingProcedure.list.length, 1)
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
