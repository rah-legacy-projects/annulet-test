require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'get operatingProcedure': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.operatingProcedure.draft(),
            testUtility.admin.operatingProcedure.update(),
            testUtility.admin.operatingProcedure.publish(),
            testUtility.operatingProcedure.list(),
            testUtility.operatingProcedure.get(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.operatingProcedure.list()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.operatingProcedure.list);
            logger.silly('[top] ' + JSON.stringify(p.operatingProcedure.list, null, 3));
            test.ok(_.all(p.operatingProcedure.list, function(op){
                return !!op.shortName;
            }));
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }

};
