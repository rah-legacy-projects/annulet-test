module.exports = exports = {
	get: require("./get"),
	instanceName: require("./instanceName"),
	listAfterDelete: require("./listAfterDelete"),
	list: require("./list"),
};
