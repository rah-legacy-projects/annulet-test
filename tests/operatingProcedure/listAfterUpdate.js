require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list ops after update': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.operatingProcedure.draft(),
            testUtility.admin.operatingProcedure.update(),
            testUtility.admin.operatingProcedure.publish(),
            testUtility.operatingProcedure.list(),
            function(p, cb){
                testUtility.admin.operatingProcedure.draft({
                    operatingProcedureDefinition: p.admin.operatingProcedure.published._id                                        
                })(p, cb);
            },
            testUtility.admin.operatingProcedure.update({title:'testing alternate'}),
            testUtility.admin.operatingProcedure.publish(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.operatingProcedure.list(),
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.operatingProcedure.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.operatingProcedure.list);
            //hack: for now, assume one
            test.equal(p.operatingProcedure.list.length, 2);
            test.equal(p.operatingProcedure.list[1].title, 'testing alternate');
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
