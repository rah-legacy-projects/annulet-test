require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list alerts after delete': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.alert.draft(),
            testUtility.admin.alert.update(),
            testUtility.admin.alert.publish(),
            testUtility.alert.list(),
            function(p, cb) {
                logger.silly(JSON.stringify(p, null, 3));
                //close the published draft
                testUtility.admin.alert.close({
                    alertDefinition: p.admin.alert.published._id
                })(p, cb);
            },

            function(p, cb) {
                logger.silly(new Array(96)
                    .join('#'));
                cb(null, p);
            },
            testUtility.alert.list(),
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.alert.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.alert.list);
            //hack: for now, assume one
            test.equal(p.alert.list.length, 0)
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
