require('../../test-config');

var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    util =require('util'),
    modeler = require('annulet-models')
    .modeler,
    testUtility = require("../../util");

module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        testUtility.setupHelper(function(err, p) {
            self.p = p;
            cb(err);
        });
    },
    'list auxiliaryDocuments': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            testUtility.admin.auxiliaryDocument.draft(),
            testUtility.admin.auxiliaryDocument.update(),
            testUtility.admin.auxiliaryDocument.publish(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.auxiliaryDocument.list()
        ], function(err, p) {
            logger.silly('[top] ' + util.inspect(p.auxiliaryDocument.list));
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.auxiliaryDocument.list);
            //hack: for now, assume one
            test.equal(p.auxiliaryDocument.list.length, 1)
            test.done();
        });
    },
    'list auxiliaryDocuments, none to list': function(test) {
        async.waterfall([

            function(cb) {
                cb(null, {});
            },
            testUtility.fakeSignup(),
            testUtility.login(),
            function(p, cb){
                logger.silly(new Array(96).join('#'));
                cb(null, p);
            },
            testUtility.auxiliaryDocument.list()
        ], function(err, p) {
            test.ok(p);
            test.ok(p.signupResponse);
            test.ok(p.authToken);
            test.ok(p.auxiliaryDocument.list);
            //hack: for now, assume one
            test.equal(p.auxiliaryDocument.list.length, 0)
            test.done();
        });
    },
    tearDown: function(cb) {
        var self = this;
        testUtility.endServices(self.p.services, function(err, p) {
            modeler.forceDisconnect(function(dcerr) {
                cb(err || dcerr);
            });
        });
    }
};
