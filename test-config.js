var logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    //level: 'warn',
    //level: 'error',
    //level: 'off',
    colorize: true
});
require('./util/errorHandler');

process.env.ENV = 'automatedTest';
