require('../test-config');

var child = require('child_process'),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('lodash'),
    request = require('request'),
    config = require('annulet-config'),
    moment = require('moment'),
    FindFiles = require('node-find-files'),
    logger = require('winston');


var checker = function(options, cb) {
    async.waterfall([

        function(cb) {
            var finder = new FindFiles({
                rootFolder: options.installedPath,
                filterFunction: function(path, stat) {
                    return !(/relic|\.bin/.test(path));
                }
            });

            var closestDate = moment('1970-01-01', 'YYYY-MM-DD');
            finder.on('match', function(path, stat) {
                closestDate = (moment(stat.mtime)
                    .isAfter(moment(closestDate)) ? moment(stat.mtime) : closestDate);
            });

            finder.on('complete', function() {
                cb(null, closestDate);
            });

            finder.on('pathError', function(err, path) {
                logger.error('error at ' + path + ': ' + util.inspect(err));
            });

            finder.on('error', function(err) {
                logger.error('error in finder: ' + util.inspect(err));
            });

            finder.startSearch();
        },
        function(closestDate, cb) {
            var finder = new FindFiles({
                rootFolder: options.projectPath,
                filterFunction: function(path, stat) {
                    return true;
                }
            });

            var projectUpdate = moment('1970-1-1');
            finder.on('match', function(path, stat) {
                projectUpdate = (moment(stat.mtime)
                    .isAfter(moment(projectUpdate)) ? moment(stat.mtime) : projectUpdate);
            });

            finder.on('complete', function() {
                logger.silly(options.prefix + ' closest date: ' + closestDate.toDate());
                logger.silly(options.prefix + ' project update: ' + projectUpdate.toDate());
                cb(null, (closestDate.isBefore(projectUpdate) || !!process.env.FORCE));
            });

            finder.on('pathError', function(err, path) {
                logger.error('error at ' + path + ': ' + util.inspect(err));
            });

            finder.on('error', function(err) {
                logger.error('error in finder: ' + util.inspect(err));
            });

            finder.startSearch();
        }
    ], function(err, update) {
        if (update) {
            logger.warn('updating ' + options.prefix + ' service');
            var idService = child.spawn('npm', ['install', path.resolve(__dirname, options.projectPath)], {
                cwd: path.resolve(__dirname, '../'),
            });
            idService.on('close', function() {
                logger.info(options.prefix + ' service updated');
                cb(err, update);
            });

            idService.on('exit', function(code, signal){
                logger.info(options.prefix + ' service exited with ' + code + ' ('+signal+')');
            });
            idService.on('message', function(message, sendHandle){
                logger.info('message from ' + options.prefix + ': ' + util.inspect(message));
            });
            idService.on('disconnect', function(){
                logger.error(options.prefix + ' disconnected?!');
            });

            idService.stdout.on('data', function(data) {
                logger.silly('[' + options.prefix + ' update] ' + data.slice(0, -1));
            });
            idService.stderr.on('data', function(data){
                logger.warn('[' + options.prefix + ' update] ' + data);;
            });
            idService.on('error', function(err) {
                logger.error('[' + options.prefix + ' update]' + util.inspect(err));
            });
        } else {
            logger.info(options.prefix + ' is up to date.');
            cb(err, update);
        }
    });
};


module.exports = exports = function(cb) {
    async.waterfall([

        function(cb) {
            var p = {};
            logger.info('updating services');
            cb(null, p);
        },
        function(p, cb) {

            async.parallel({
                id: function(cb) {
                    checker({
                        prefix: 'id',
                        projectPath: path.resolve(__dirname, '../../annulet-id'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-id')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
                ui: function(cb) {
                    checker({
                        prefix: 'ui',
                        projectPath: path.resolve(__dirname, '../../annulet-ui'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-ui')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
                api: function(cb) {
                    checker({
                        prefix: 'api',
                        projectPath: path.resolve(__dirname, '../../annulet-api'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-api')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
                util: function(cb) {
                    checker({
                        prefix: 'util',
                        projectPath: path.resolve(__dirname, '../../annulet-util'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-util')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
                content: function(cb) {
                    checker({
                        prefix: 'content',
                        projectPath: path.resolve(__dirname, '../../annulet-content'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-content')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
                models: function(cb) {
                    checker({
                        prefix: 'models',
                        projectPath: path.resolve(__dirname, '../../annulet-models'),
                        installedPath: path.resolve(__dirname, '../node_modules/annulet-models')
                    }, function(err, r) {
                        cb(err, r);
                    });
                },
            }, function(err, r) {
                logger.silly('update complete: ' + util.inspect(r));
                cb(err, r);
            })
        }
    ], function(err, p) {
        logger.silly('calling back from update');
        cb(err, p);
    });
};
