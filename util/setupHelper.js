var logger = require('winston'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models').modeler,
    config = require('annulet-config'),

    dropDatabase = require('./dropDatabase'),
    startServices = require('./startServices'),
    update = require('./update');

module.exports = exports = function(cb) {
    async.waterfall([
        function(cb) {
            logger.silly('[setup helper] dropping db');
            dropDatabase(function(err) {
                cb(err, {});
            });
        },
        function(p, cb){
            logger.silly('[setup helper] updating services');
            update(function(err){
                cb(err, p);
            });
        },
        function(p, cb){
            logger.silly('[setup helper] running modeler setup');
            modeler.setup(config.configuration.paths.dbListing(), function(err){
                cb(err, p);
            });
        },
        function(p, cb) {
            logger.silly('[setup helper] starting services');
            startServices(function(err, services) {
                p.services = services;
                cb(err, p);
            });
        },
    ], function(err, p) {
        logger.silly('[setup helper] setup helper complete');
        cb(err, p);
    });
};
