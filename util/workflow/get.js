var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            workflow: p.workflow.list[0].definitionId
        });
        p = _.defaultsDeep(p, {
            workflow: {}
        });
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                logger.silly('[wf get] id: ' + options.workflow);
                logger.silly('[wf get] customer: ' + p.signupResponse.customer._id);
                logger.silly('[wf get] url: ' + config.configuration.paths.apiUri() + '/workflow/'+options.workflow);
                request.get({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/workflow/' + options.workflow,
                    headers: {
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    }
                }, function(err, response, body) {
                    logger.silly('[wf get] response: ' + util.inspect(body));
                    p.workflow.get = body.data;
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
