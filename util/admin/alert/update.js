var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            markdown: 'test alert',
            alertLevel: 'info'
        });
        p = _.defaultsDeep(p, {admin: { alert: {}}});
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/alert/update',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        markdown: options.markdown,
                        alertLevel: options.alertLevel,
                        startDate: moment().toDate(),
                        endDate: moment().add(1, 'months').toDate(),
                        _id: p.admin.alert.draft._id,
                        _rangeId: p.admin.alert.draft.rangedData[0]._id,

                    }
                }, function(err, response, body) {
                    p.admin.alert.update = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
