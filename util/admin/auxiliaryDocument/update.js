var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    ObjectId = require('mongoose').Types.ObjectId,
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            displayName: 'test auxiliaryDocument',
            description: 'test description',
            extension: 'pdf'
        });
        p = _.defaultsDeep(p, {admin: { auxiliaryDocument: {}}});
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/auxiliaryDocument/update',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        displayName: options.displayName,
                        file: new ObjectId(),
                        description: options.description,
                        extension: options.extension,
                        startDate: moment().toDate(),
                        endDate: moment().add(1, 'months').toDate(),
                        _id: p.admin.auxiliaryDocument.draft._id,
                        _rangeId: p.admin.auxiliaryDocument.draft.rangedData[0]._id,

                    }
                }, function(err, response, body) {
                    p.admin.auxiliaryDocument.update = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
