var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    ObjectId = require('mongoose').Types.ObjectId,
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            title: null,
            quizDefinition: null, 
            quizDefinitionRange: null,
            startDate: moment().toDate(),
            endDate: moment().add(30, 'days').toDate()
        });
        p = _.defaultsDeep(p, {admin: { quiz: {}}});
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/quiz/update',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        title: options.title,
                        _id: p.admin.quiz.draft._id,
                        _rangeId: p.admin.quiz.draft.rangedData[0]._id,
                        startDate: options.startDate,
                        endDate: options.endDate

                    }
                }, function(err, response, body) {
                    p.admin.quiz.update = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
