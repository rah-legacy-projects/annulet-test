var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        p = _.defaultsDeep(p, {
            admin: {
                quiz: {}
            }
        });

        options = _.defaults(options || {}, {
            quizDefinition: null
        });
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/quiz/close',
                    headers: {
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        _id: options.quizDefinition || 'new',
                    }
                }, function(err, response, body) {
                    p.admin.quiz.close = body.data;
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
