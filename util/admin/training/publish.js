var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            trainingDefinition: p.admin.training.draft._id
        });
        p = _.defaultsDeep(p, {admin: { training: {}}});
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/training/publish',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body:{
                        _id: options.trainingDefinition
                    }
                }, function(err, response, body) {
                    p.admin.training.published = body.data;
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
