var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            title: 'test operatingProcedure'
        });
        p = _.defaultsDeep(p, {admin: { operatingProcedure: {}}});
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/operatingProcedure/update',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        title: options.title,
                        description: 'some autotest procedure',
                        operatingProcedureLevel: 'info',
                        startDate: moment().toDate(),
                        endDate: moment().add(1, 'months').toDate(),
                        _id: p.admin.operatingProcedure.draft._id,
                        _rangeId: p.admin.operatingProcedure.draft.rangedData[0]._id,

                    }
                }, function(err, response, body) {
                    p.admin.operatingProcedure.update = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
