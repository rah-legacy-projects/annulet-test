var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    ObjectId = require('mongoose').Types.ObjectId,
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            _id: p.admin.workflow.draft._id,
            _rangeId: p.admin.workflow.draft._rangeId,
            title: 'test'
        });
        p = _.defaultsDeep(p, {
            admin: {
                workflow: {}
            }
        });
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.post({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/admin/workflow/update',
                    headers: {
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    },
                    body: {
                        startDate: moment()
                            .startOf('day')
                            .toDate(),
                        endDate: 'forever',
                        employeeClass: ['test'],
                        _id: options._id,
                        _rangeId: options._rangeId,
                        title: options.title,
                        description: 'test description',
                        shortName: 'test',
                        __t: 'definitions.workflow.workflowItemTypes.Root',
                        _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Root',
                        items: [{
                            title: 'container',
                            __t: 'definitions.workflow.workflowItemTypes.Container',
                            _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Container',
                            sequence: [{
                                title: 'training',
                                __t: 'definitions.workflow.workflowItemTypes.Training',
                                _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Training',
                                training: new ObjectId()
                            }, {
                                title: 'quiz',
                                __t: 'definitions.workflow.workflowItemTypes.Quiz',
                                _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Quiz',
                                quiz: new ObjectId()
                            }]
                        }]
                    }
                }, function(err, response, body) {
                    p.admin.workflow.update = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
