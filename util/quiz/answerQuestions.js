var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            quiz: p.quiz.get
        });
        p = _.defaultsDeep(p, {
            quiz: {}
        });
        async.waterfall([

                function(cb) {
                    cb(null, p);
                },
                function(p, cb) {
                    async.series(_.map(options.quiz.questions, function(question) {
                            return function(cb) {
                                logger.silly('question: ' + util.inspect(question));

                                var body = {};
                                if (question.__t == 'instances.quiz.questionTypes.TrueFalse') {
                                    body.answers = question.correctAnswer;
                                } else if (question.__t == 'instances.quiz.questionTypes.MultipleChoice') {
                                    body.answers = _.chain(question.answers)
                                        .filter(function(answer) {
                                            return answer.isCorrect;
                                        })
                                        .map(function(answer) {
                                            return {
                                                _id: answer._id
                                            };
                                        })
                                        .value();
                                } else if (question.__t == 'instances.quiz.questionTypes.OneChoice') {
                                    var answer = _.find(question.answers, function(answer){
                                        return answer.isCorrect;
                                    });
                                    body.answers = {
                                        _id: answer._id
                                    }
                                }

                                logger.warn('%%% hay, about to answer qusetion');
                                request.post({
                                    json: true,
                                    url: config.configuration.paths.apiUri() + '/quiz/answer/' + question.__t.split('.')
                                        .reverse()[0] + '/' + question._id,
                                    headers: {
                                        'annulet-auth-token': p.authToken,
                                        'annulet-auth-customer': p.signupResponse.customer._id
                                    },
                                    body: body
                                }, function(err, response, body) {
                                    logger.silly('%%% qusetion answerered: ' + util.inspect(body));
                                    cb(err, body.data);
                                });
                            }
                        }),
                        function(err, r) {
                            p.quiz.answers = r;
                            logger.silly('[quiz answer questions] finished saving answers');
                            cb(err, p);
                        });
                },
            ],
            function(err, p) {
                cb(err, p);
            });
    };
};
