var child = require('child_process'),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('lodash'),
    request = require('request'),
    config = require('annulet-config'),
    logger = require('winston');

var MAX_TRIES = 100000000;
module.exports = exports = function(cb) {
    async.waterfall([

        function(cb) {
            var p = {};
            logger.info('starting test services');
            cb(null, p);
        },
        function(p, cb) {
            //start id service
            p.idService = child.spawn('node', ['app.js'], {
                cwd: path.resolve(__dirname, '../node_modules/annulet-id'),
                env: {
                    ENV: process.env.ENV
                }
            });
            p.idService.on('error', function(err) {
                logger.error(util.inspect(err));
            });
            p.idService.on('close', function() {
                logger.info('ID service closed.');
            });
            
            var lastLog = 'silly';
            p.idService.stdout.on('data', function(data) {
                var loggerre = /.*?(silly|debug|info|warn|error).*?\:\w*([^]+)/;
                if (loggerre.test(data.toString())) {
                    var msg = loggerre.exec(data.toString());
                    lastLog = msg[1];
                    logger.log(msg[1], '[ID SRV] ' + msg[2].slice(0, -1));
                }
                else{
                    logger.log(lastLog, '[ID SRV] ' + data.toString().slice(0, -1));
                }
            });
            p.idService.stderr.on('data', function(data) {
                logger.error('[ID SRV] ' + data.toString());
            });

            logger.info('ID service started.');
            cb(null, p);
        },
        function(p, cb) {
            logger.silly('starting api: ' + path.resolve(__dirname, '../node_modules/annulet-api') + ' in env ' + process.env.ENV);
            //start api service
            p.apiService = child.spawn('node', ['app.js'], {
                cwd: path.resolve(__dirname, '../node_modules/annulet-api').toString(),
                env: {
                    ENV: process.env.ENV
                }
            });
            p.apiService.on('error', function(err) {
                logger.error(util.inspect(err));
            });
            p.apiService.on('close', function() {
                logger.info('API service closed.');
            });

            var lastLog = 'silly';
            p.apiService.stdout.on('data', function(data) {
                var loggerre = /.*?(silly|debug|info|warn|error).*?\:\w*([^]+)/;
                if (loggerre.test(data.toString())) {
                    var msg = loggerre.exec(data.toString());
                    lastLog = msg[1];
                    logger.log(msg[1], '[API SRV] ' + msg[2].slice(0, -1));
                }
                else{
                    logger.log(lastLog, '[API SRV] ' + data.toString().slice(0, -1));
                }
            });
    
            p.apiService.stderr.on('data', function(data) {
                logger.error('[API SRV] ' + data.toString());
            });

            logger.info('API service started.');
            cb(null, p);
        },
        function(p, cb) {
            //start ui service
            p.uiService = child.spawn('node', ['app.js'], {
                cwd: path.resolve(__dirname, '../node_modules/annulet-ui'),
                env: {
                    ENV: process.env.ENV
                }
            });
            p.uiService.on('error', function(err) {
                logger.error(util.inspect(err));
            });
            p.uiService.on('close', function() {
                logger.info('UI service closed.');
            });
            logger.info('UI service started.');

            p.uiService.stdout.on('data', function(data) {
                var loggerre = /.*?(silly|debug|info|warn|error).*?\:\w*([^]+)/;
                if (loggerre.test(data.toString())) {
                    var msg = loggerre.exec(data.toString());
                    lastLog = msg[1];
                    logger.log(msg[1], '[UI SRV] ' + msg[2].slice(0, -1));
                }
                else{
                    logger.log(lastLog, '[UI SRV] ' + data.toString().slice(0, -1));
                }
            });
            p.uiService.stderr.on('data', function(data) {
                logger.error('[UI SRV] ' + data.toString());
            });

            cb(null, p);
        },
        function(p, cb) {
            logger.silly('ensuring services ready');
            //ensure all three services are ready
            async.parallel({
                id: function(cb) {
                    var responseStatus = null;
                    var tries = 0;
                    async.until(
                        function() {
                            return responseStatus == 200
                        },
                        function(cb) {
                            setTimeout(function() {
                                request.get({
                                    url: config.configuration.paths.authUri() + '/health',
                                    json: true
                                }, function(err, response, body) {
                                    responseStatus = (response || {})
                                        .statusCode;
                                    if (!!err && err.code == 'ECONNREFUSED') {
                                        tries++;
                                        if (tries > MAX_TRIES) {
                                            return cb('Tries exceeded for ID');
                                        }
                                        cb(null);
                                    } else {
                                        cb(err);
                                    }
                                });
                            }, 250);
                        },
                        function(err) {
                            if (!err) {
                                logger.silly('ID service is up and healthy ' + tries);
                            }
                            cb(err);
                        });
                },
                api: function(cb) {
                    var responseStatus = null;
                    var tries = 0;
                    async.until(
                        function() {
                            return responseStatus == 200
                        },
                        function(cb) {
                            setTimeout(function() {
                                request.get({
                                    url: config.configuration.paths.apiUri() + '/health',
                                    json: true
                                }, function(err, response, body) {
                                    responseStatus = (response || {})
                                        .statusCode;
                                    if (!!err && err.code == 'ECONNREFUSED') {
                                        tries++;
                                        if (tries > MAX_TRIES) {
                                            return cb('Tries exceeded for API');
                                        }
                                        cb(null);
                                    } else {
                                        cb(err);
                                    }
                                });
                            }, 250);
                        },
                        function(err) {
                            if (!err) {
                                logger.silly('API service is up and healthy ' + tries);
                            }
                            cb(err);
                        });
                },
                ui: function(cb) {
                    var responseStatus = null;
                    var tries = 0;
                    async.until(
                        function() {
                            return responseStatus == 200
                        },
                        function(cb) {
                            setTimeout(function() {
                                request.get({
                                    url: config.configuration.paths.uiUri() + '/health',
                                    json: true
                                }, function(err, response, body) {
                                    responseStatus = (response || {})
                                        .statusCode;
                                    if (!!err && err.code == 'ECONNREFUSED') {
                                        tries++;
                                        if (tries > MAX_TRIES) {
                                            return cb('Tries exceeded for UI');
                                        }
                                        cb(null);
                                    } else {
                                        cb(err);
                                    }
                                });
                            }, 250);
                        },
                        function(err) {
                            if (!err) {
                                logger.silly('UI service is up and healthy ' + tries);
                            }
                            cb(err);
                        });
                },
            }, function(err, r) {
                cb(err, p);
            });
        },
    ], function(err, p) {
        if (!!err) {
            logger.error('problem setting services up: ' + util.inspect(err));
        }
        logger.info('services started');
        cb(err, p);
    });
};
