var _ = require('lodash'),
    async = require('async'),
    logger = require('winston'),
    mongoose = require('mongoose'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    content = require('annulet-content'),
    config = require('annulet-config'),
    util = require('util'),
    request = require('request');

module.exports = exports = function(options) {
    logger.silly('fake signup called');
    return function(p, cb) {
        options = options || {};
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                //sign up
                var body = {
                    user: {
                        firstName: 'autotest',
                        lastName: 'autotest',
                        password: 'autotest',
                        email: 'test@auto.test',
                        employeeClass: ['test']
                    },
                    customer: {
                        name: 'Test Company that has an excessively long, *really* long name',
                        address: {
                            street1: 'one',
                            city: 'city',
                            state: 'state',
                            zip: 'zip'
                        },
                        phone: 'phone',
                        numberOfLocations: 1,
                        numberOfEmployees: 1,
                    }
                };

                var url = config.configuration.paths.apiUri() + '/signup';
                logger.silly('[signup] url: ' + url);

                request.post({
                    json: true,
                    url: url,
                    headers: {},
                    body: body
                }, function(err, response, body) {
                    logger.silly('[signup] \t\t\t signup complete');
                    if(!body.data){
                        logger.error(util.inspect(body));
                    }
                    p.signupResponse = body.data;
                    cb(err, p);
                });
            },
        ], function(err, p) {
            logger.silly('[signup] \t\t\t signup complete');
            cb(err, p);
        });

    };
};
