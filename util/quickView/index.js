module.exports = exports = {
	adminOutstandingOPs: require("./adminOutstandingOPs"),
	adminOutstandingWorkflows: require("./adminOutstandingWorkflows"),
	userOperatingProcedures: require("./userOperatingProcedures"),
	userWorkflows: require("./userWorkflows"),
};
