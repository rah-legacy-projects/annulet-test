var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
        });
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                request.get({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/quickView/adminOutstandingWorkflows',
                    headers:{
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    }
                }, function(err, response, body) {
                    p.adminWorkflowStatusList = body.data
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
