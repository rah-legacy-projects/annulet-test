var child = require('child_process'),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    request = require('request'),
    util = require('util'),
    config = require('annulet-config'),
    logger = require('winston');
module.exports = exports = function(p, cb) {

    logger.silly('p keys: ' + util.inspect(_.keys(p)));

    p.idService.kill('SIGHUP');
    p.apiService.kill('SIGHUP');
    p.uiService.kill('SIGHUP');

    async.parallel({
        id: function(cb) {
            p.idService.on('exit', function(code, signal){
                cb(code);
            });
        },
        api: function(cb) {
            p.apiService.on('exit', function(code, signal){
                cb(code);
            });
        },
        ui: function(cb) {
            p.uiService.on('exit', function(code, signal){
                cb(code);
            });
        },
    }, function(err, r) {
        logger.silly('all services successfully stopped.');
        cb(err, p);
    });
};
