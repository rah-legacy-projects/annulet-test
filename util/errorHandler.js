
var logger = require('winston');
logger.silly('setting up uncaught exception');
process.on('uncaughtException', function(x){
    logger.error('uncaught: ' + x);
    logger.error(x.stack);
});
