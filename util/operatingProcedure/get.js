var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options) {
    return function(p, cb) {
        options = _.defaults(options || {}, {
            operatingProcedure: p.operatingProcedure.list[0].definitionId
        });
        p = _.defaultsDeep(p, {
            operatingProcedure: {}
        });
        async.waterfall([

            function(cb) {
                cb(null, p);
            },
            function(p, cb) {
                logger.silly('[op get] id: ' + options.operatingProcedure);
                request.get({
                    json: true,
                    url: config.configuration.paths.apiUri() + '/operatingProcedure/' + options.operatingProcedure,
                    headers: {
                        'annulet-auth-token': p.authToken,
                        'annulet-auth-customer': p.signupResponse.customer._id
                    }
                }, function(err, response, body) {
                    logger.silly('[op get] response: ' + util.inspect(body));
                    p.operatingProcedure.get = body.data;
                    cb(err, p);
                });
            },
        ], function(err, p) {
            cb(err, p);
        });
    };
};
